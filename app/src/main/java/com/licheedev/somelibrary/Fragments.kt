package com.licheedev.somelibrary

import android.os.Bundle
import androidx.annotation.NonNull
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.fragment.app.FragmentManager


/**
 * 创建DialogFragment，并设置空白的Bundle作为参数
 */
inline fun <reified T : DialogFragment> FragmentActivity.showDialog(): T {
    val instance = T::class.java.newInstance()
    instance.arguments = Bundle()
    instance.show(this.supportFragmentManager, instance.javaClass.name)
    return instance
}

/**
 * 创建DialogFragment，并设置空白的Bundle作为参数
 */
inline fun <reified T : DialogFragment> Fragment.showDialog(): T {
    val instance = T::class.java.newInstance()
    instance.arguments = Bundle()
    instance.show(this.childFragmentManager, instance.javaClass.name)
    return instance
}

fun DialogFragment.show(@NonNull manager: FragmentManager) {
    this.show(manager, this.javaClass.name)
}
