package com.licheedev.somelibrary;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.Observer;
import java.lang.reflect.Field;

public class MyDialogFragment extends DialogFragment {

    private Observer<LifecycleOwner> mObserver = new Observer<LifecycleOwner>() {
        @SuppressLint("SyntheticAccessor")
        @Override
        public void onChanged(LifecycleOwner lifecycleOwner) {
            if (lifecycleOwner != null && getShowsDialog()) {
                View view = requireView();
                if (view.getParent() != null) {
                    return;
                }

                Dialog dialog = getDialog();
                if (dialog != null) {
                    dialog.setContentView(view);
                }
            }
        }
    };

    @Override
    public void onAttach(@NonNull Context context) {
        changeObserver();
        super.onAttach(context);
    }

    private void changeObserver() {
        try {
            Field field = DialogFragment.class.getDeclaredField("mObserver");
            field.setAccessible(true);
            field.set(this, mObserver);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
        @Nullable Bundle savedInstanceState) {
        Dialog dialog = requireDialog();
        return getDialogRootView(dialog);
    }

    protected View getDialogRootView(Dialog dialog) {
        return ((ViewGroup) dialog.findViewById(android.R.id.content)).getChildAt(0);
    }
}
