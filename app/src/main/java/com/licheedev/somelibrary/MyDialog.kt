package com.licheedev.somelibrary

import android.os.Bundle
import android.os.Handler
import android.view.View
import androidx.lifecycle.MutableLiveData
import com.licheedev.myutils.LogPlus


class MyDialog : MyCommonDialogFragment() {


    private val liveData = MutableLiveData<Int>(0);

    override fun getLayoutId(): Int {
        LogPlus.e("进来了")


        return R.layout.dialog_custom
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        LogPlus.e("进来了")

        liveData.observe(viewLifecycleOwner) {
            LogPlus.e(it.toString())
        }

        Handler().postDelayed({
            liveData.value = 2000
        }, 2000L)

    }
}