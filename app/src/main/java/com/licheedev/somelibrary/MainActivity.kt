package com.licheedev.somelibrary

import android.os.Bundle
import android.view.View
import com.licheedev.base.CommonActivity

class MainActivity : CommonActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        setTranslucentStatus()
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        findViewById<View>(R.id.button).setOnClickListener { 
            
            showDialog<MyDialog>()
            
        }
    }
}