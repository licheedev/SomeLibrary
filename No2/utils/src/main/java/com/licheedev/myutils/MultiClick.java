package com.licheedev.myutils;

import android.os.SystemClock;
import android.util.SparseArray;
import android.view.View;

/**
 * Created by John on 2018/8/15.
 */
public class MultiClick {

    private final SparseArray<Param> mParams;

    /**
     * 参数
     */
    private static class Param {
        private long[] times;
        private long duration;
        private long timeout;
        private int count;

        Param(int destTimes, long duration, long timeout) {
            this.times = new long[destTimes];
            this.duration = duration;
            this.timeout = timeout;
            this.count = 0;
        }
    }

    public interface MultiClickListener {

        /**
         * 当被多次点击时
         *
         * @param view
         * @param currentCount 当前点击次数
         * @param destTimes 目标点击次数
         */
        void onMultiClick(View view, int currentCount, int destTimes);

        /**
         * 当成功进行多次点击时
         *
         * @param view
         */
        void onMultiClickSuccess(View view);
    }

    private MultiClickListener mListener;

    /**
     * 多次点击
     */
    public MultiClick() {
        mParams = new SparseArray<>();
    }

    /**
     * 设置监听器
     *
     * @param listener
     */
    public void setListener(MultiClickListener listener) {
        mListener = listener;
    }

    /**
     * 注册需要多次点击的控件
     *
     * @param view 需要绑定多次点击的控件
     * @param destTimes 点击次数
     * @param duration 判断多次点击是否成功的最大事件
     * @param timeout 两次点击之间的最大时间差
     */
    public void register(View view, int destTimes, long duration, long timeout) {
        Param param = new Param(destTimes, duration, timeout);
        mParams.put(view.getId(), param);
    }

    /**
     * 点击一次
     *
     * @param view
     */
    public void click(View view) {
        Param param = mParams.get(view.getId());
        if (param != null) {
            handleClick(view, param);
        }
    }

    /**
     * 处理点击
     *
     * @param view
     * @param param
     */
    private void handleClick(View view, Param param) {

        param.count++;

        for (int i = param.count - 1; i > 0; i--) {
            param.times[i] = param.times[i - 1];
        }

        param.times[0] = SystemClock.uptimeMillis();

        //LogPlus.e(Arrays.toString(param.times));

        long offset = param.times[0] - param.times[1];
        long allOffset = param.times[0] - param.times[param.count - 1];
        if (param.count > 1) {
            if (offset > param.timeout || allOffset > param.duration) {
                param.count = 1;
            } else {
                if (param.count == param.times.length) {
                    param.count = 0;
                    if (mListener != null) {
                        mListener.onMultiClickSuccess(view);
                    }
                    return;
                }
            }
        }

        if (mListener != null) {
            mListener.onMultiClick(view, param.count, param.times.length);
        }
    }
}
