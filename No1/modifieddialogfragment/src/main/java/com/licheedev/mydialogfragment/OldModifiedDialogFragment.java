package com.licheedev.mydialogfragment;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.annotation.MainThread;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import java.lang.reflect.Field;

/**
 * 修改过的DialogFragment,只需要重写 {@link #onCreateDialog(Bundle)}, {@link #onCreateView(LayoutInflater, ViewGroup, Bundle)} 直接返回Dialog的rootView，而且运行时不会崩溃
 */
public class OldModifiedDialogFragment extends DialogFragment {

    private final DialogInterface.OnCancelListener mOnCancelListener =
        new DialogInterface.OnCancelListener() {
            @SuppressLint("SyntheticAccessor")
            @Override
            public void onCancel(@Nullable DialogInterface dialog) {
                Dialog mDialog = getDialog();
                if (mDialog != null) {
                    OldModifiedDialogFragment.this.onCancel(mDialog);
                }
            }
        };

    private final DialogInterface.OnDismissListener mOnDismissListener =
        new DialogInterface.OnDismissListener() {
            @SuppressLint("SyntheticAccessor")
            @Override
            public void onDismiss(@Nullable DialogInterface dialog) {
                Dialog mDialog = getDialog();
                if (mDialog != null) {
                    OldModifiedDialogFragment.this.onDismiss(mDialog);
                }
            }
        };

    // TODO 待实现
    // @NonNull
    //@Override
    //public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
    //    return super.onCreateDialog(savedInstanceState);
    //}

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
        @Nullable Bundle savedInstanceState) {
        Dialog dialog = requireDialog();
        return getDialogRootView(dialog);
    }

    protected View getDialogRootView(Dialog dialog) {
        return ((ViewGroup) dialog.findViewById(android.R.id.content)).getChildAt(0);
    }

    @MainThread
    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        try {
            // 先调用一下super，如无意外会抛出异常
            super.onActivityCreated(savedInstanceState);
        } catch (Exception e) {
            //e.printStackTrace();
            // 执行修改过代码
            Dialog mDialog = getDialog();

            if (!getShowsDialog()) {
                return;
            }

            if (mDialog != null) {
                // 因为 onCreateView()中已经直接返回Dialog的rootView了,所以这里就不用 mDialog.setContentView()了
                //View view = getView();
                //if (view != null) {
                //    if (view.getParent() != null) {
                //        throw new IllegalStateException(
                //            "DialogFragment can not be attached to a container view");
                //    }
                //    mDialog.setContentView(view);
                //}
                final Activity activity = getActivity();
                if (activity != null) {
                    mDialog.setOwnerActivity(activity);
                }
                mDialog.setCancelable(isCancelable());
                mDialog.setOnCancelListener(mOnCancelListener);
                mDialog.setOnDismissListener(mOnDismissListener);
                if (savedInstanceState != null) {
                    Bundle dialogState = savedInstanceState.getBundle(SAVED_DIALOG_STATE_TAG());
                    if (dialogState != null) {
                        mDialog.onRestoreInstanceState(dialogState);
                    }
                }
            }
        }
    }

    protected String SAVED_DIALOG_STATE_TAG() {
        try {
            Field field = DialogFragment.class.getDeclaredField("SAVED_DIALOG_STATE_TAG");
            field.setAccessible(true);
            return (String) field.get(null);
        } catch (Exception e) {
            e.printStackTrace();
            return "android:savedDialogState";
        }
    }
}
