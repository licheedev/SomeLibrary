package com.licheedev.labelview;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import androidx.annotation.DrawableRes;
import androidx.annotation.StringRes;
import androidx.constraintlayout.widget.ConstraintLayout;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

public class LabelTextView extends ConstraintLayout {

    public final TextView label;
    public final TextView value;
    public final View line;
    public final ImageView icon;
    public final ImageView icon2;

    public LabelTextView(Context context) {
        this(context, null);
    }

    public LabelTextView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public LabelTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

        int layoutId = R.layout.label_text_view_default_layout;
        int labelId = R.id.tv_label;
        int valueId = R.id.tv_value;
        int lineId = R.id.line;
        int iconId = R.id.iv_icon;
        int icon2Id = 0; // 这个一般没有

        String labelString = null;
        String valueString = null;

        int iconDrawable = 0;
        int icon2Drawable = 0;

        int[] VISIBILITY = { View.VISIBLE, View.INVISIBLE, View.GONE };

        int labelVisibility = VISIBILITY[0];
        int valueVisibility = VISIBILITY[0];
        int lineVisibility = VISIBILITY[0];
        int iconVisibility = VISIBILITY[0];
        int icon2Visibility = VISIBILITY[0];

        if (attrs != null) {
            final TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.LabelTextView);
            int N = a.getIndexCount();
            for (int i = 0; i < N; i++) {
                int attr = a.getIndex(i);
                // 覆盖布局
                if (attr == R.styleable.LabelTextView_override_layout) {
                    layoutId = a.getResourceId(attr, layoutId);
                } else if (attr == R.styleable.LabelTextView_override_label_id) {
                    labelId = a.getResourceId(attr, labelId);
                } else if (attr == R.styleable.LabelTextView_override_value_id) {
                    valueId = a.getResourceId(attr, valueId);
                } else if (attr == R.styleable.LabelTextView_override_line_id) {
                    lineId = a.getResourceId(attr, lineId);
                } else if (attr == R.styleable.LabelTextView_override_icon_id) {
                    iconId = a.getResourceId(attr, iconId);
                } else if (attr == R.styleable.LabelTextView_override_icon2_id) {
                    icon2Id = a.getResourceId(attr, icon2Id);
                }
                // 可见性
                else if (attr == R.styleable.LabelTextView_label_visibility) {
                    labelVisibility = a.getInt(attr, 0);
                } else if (attr == R.styleable.LabelTextView_value_visibility) {
                    valueVisibility = a.getInt(attr, 0);
                } else if (attr == R.styleable.LabelTextView_line_visibility) {
                    lineVisibility = a.getInt(attr, 0);
                } else if (attr == R.styleable.LabelTextView_icon_visibility) {
                    iconVisibility = a.getInt(attr, 0);
                } else if (attr == R.styleable.LabelTextView_icon2_visibility) {
                    icon2Visibility = a.getInt(attr, 0);
                }
                // 文本
                else if (attr == R.styleable.LabelTextView_label_text) {
                    labelString = a.getString(attr);
                } else if (attr == R.styleable.LabelTextView_value_text) {
                    valueString = a.getString(attr);
                }
                // 图标
                else if (attr == R.styleable.LabelTextView_icon_drawable) {
                    iconDrawable = a.getResourceId(attr, 0);
                } else if (attr == R.styleable.LabelTextView_icon2_drawable) {
                    icon2Drawable = a.getResourceId(attr, 0);
                }
            }
            a.recycle();
        }

        LayoutInflater.from(context).inflate(layoutId, this, true);
        this.label = findViewById(labelId);
        this.value = findViewById(valueId);
        this.line = findViewById(lineId);
        this.icon = findViewById(iconId);
        this.icon2 = findViewById(icon2Id);

        setViewVisibility(this.label, VISIBILITY[labelVisibility]);
        setViewVisibility(this.value, VISIBILITY[valueVisibility]);
        setViewVisibility(this.line, VISIBILITY[lineVisibility]);
        setViewVisibility(this.icon, VISIBILITY[iconVisibility]);
        setViewVisibility(this.icon2, VISIBILITY[icon2Visibility]);

        setLabelText(labelString);
        setValueText(valueString);
        if (iconDrawable != 0) {
            setIconImageRes(iconDrawable);
        }
        if (icon2Drawable != 0) {
            setIcon2ImageRes(icon2Drawable);
        }
    }

    private void setText(TextView textView, @StringRes int resid) {
        if (textView != null) {
            textView.setText(resid);
        }
    }

    private void setText(TextView textView, CharSequence text) {
        if (textView != null) {
            textView.setText(text);
        }
    }

    private void setViewVisibility(View view, int visibility) {
        if (view != null) {
            view.setVisibility(visibility);
        }
    }

    /**
     * 设置标题文本
     *
     * @param text
     */
    public void setLabelText(CharSequence text) {
        setText(this.label, text);
    }

    /**
     * 设置标题文本
     *
     * @param resid
     */
    public void setLabelText(@StringRes int resid) {
        setText(this.label, resid);
    }

    /** 获取标题文本 */
    public String getLabelText() {
        if (this.label == null) {
            return "";
        } else {
            return label.getText().toString();
        }
    }

    /**
     * 设置数值文本
     *
     * @param text
     */
    public void setValueText(CharSequence text) {
        setText(this.value, text);
    }

    /**
     * 设置数值文本
     *
     * @param resid
     */
    public void setValueText(@StringRes int resid) {
        setText(this.value, resid);
    }

    /** 获取数值文本 */
    public String getValueText() {
        if (this.value == null) {
            return "";
        } else {
            return value.getText().toString();
        }
    }

    /**
     * 设置图标
     *
     * @param resid
     */
    public void setIconImageRes(@DrawableRes int resid) {
        if (icon != null) {
            icon.setImageResource(resid);
        }
    }

    /**
     * 设置图标
     *
     * @param drawable
     */
    public void setIconDrawable(Drawable drawable) {
        if (icon != null) {
            icon.setImageDrawable(drawable);
        }
    }

    /**
     * 设置图标2
     *
     * @param resid
     */
    public void setIcon2ImageRes(@DrawableRes int resid) {
        if (icon2 != null) {
            icon2.setImageResource(resid);
        }
    }

    /**
     * 设置图标2
     *
     * @param drawable
     */
    public void setIcon2Drawable(Drawable drawable) {
        if (icon2 != null) {
            icon2.setImageDrawable(drawable);
        }
    }
}
