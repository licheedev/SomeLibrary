# SomeLibrary

```gradle
allprojects {
  repositories {
    ...
    mavenCentral()
  }
}


// 包含base、utils、clickablebg、rxjava2ex、rxjava3ex、widgets
implementation("com.licheedev:some-commonlibrary:1.0.19") {
    // 根据需要，排除掉不需要的rxjava版本，可以两个同时排除掉
    exclude group: 'com.licheedev', module: 'some-rxjava2ex'
    exclude group: 'com.licheedev', module: 'some-rxjava3ex'
}
// 其他组件
implementation 'com.licheedev:some-clickablebg:1.0.0'
implementation 'com.licheedev:some-context:1.0.0'
implementation 'com.licheedev:some-labelview:1.0.2'
implementation 'com.licheedev:some-modifieddialogfragment:1.0.1'
implementation 'com.licheedev:some-rateview:1.0.1'
implementation 'com.licheedev:some-rxjava2ex:1.0.1'
implementation 'com.licheedev:some-rxjava3ex:1.0.1'
implementation 'com.licheedev:some-widgets:1.0.6'

implementation 'com.licheedev:some-utils:1.0.5'
implementation 'com.licheedev:some-statuslayout:1.0.2'

implementation 'com.licheedev:some-base:1.0.14'
```


编译失败就加入：
```
android {
    ...
    compileOptions {
        sourceCompatibility JavaVersion.VERSION_1_8
        targetCompatibility JavaVersion.VERSION_1_8
    }
}
```
# 其他常用依赖
Jetpack
> https://developer.android.google.cn/jetpack/androidx/explorer?hl=zh-cn

Lifecycle
> https://developer.android.google.cn/jetpack/androidx/releases/lifecycle?hl=zh-cn#declaring_dependencies

数据绑定
> https://developer.android.google.cn/topic/libraries/data-binding/start?hl=zh-cn
```gradle
android {
        ...
        dataBinding {
            enabled = true
        }
    }
```

Glide
> https://github.com/bumptech/glide
```gradle
implementation 'com.github.bumptech.glide:glide:4.11.0'
annotationProcessor 'com.github.bumptech.glide:compiler:4.11.0'
```
LogPlus
> https://github.com/licheedev/LogPlus
```gradle
implementation 'com.licheedev:logplus:1.0.0'
```

ByteUtils、SystemClockEx等
> https://github.com/licheedev/HardwareUtils
```gradle
implementation 'com.licheedev:hardwareutils:1.0.0'
```

一些kotlin和jetpack等相关工具类
> https://gitee.com/licheedev/some-ext

RxHttp扩展
> https://gitee.com/licheedev/rx-http-extension-demo

