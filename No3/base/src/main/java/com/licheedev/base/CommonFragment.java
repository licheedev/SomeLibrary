package com.licheedev.base;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import androidx.fragment.app.Fragment;
import com.licheedev.base.core.BindEventBus;
import com.licheedev.base.core.UiView;
import com.licheedev.myutils.LogPlus;
import org.greenrobot.eventbus.EventBus;

/**
 * 基础Fragment，建议继承此类再写一个BaseFragment
 */
public abstract class CommonFragment extends Fragment implements UiView {


    private Activity mActivity;

    @Override
    public void onAttach(Context context) {
        mActivity = (Activity) context;
        super.onAttach(context);
    }
    
    /**
     * 需要注册EventBus
     *
     * @return
     */
    protected boolean toRegisterEventBus() {
        return this.getClass().isAnnotationPresent(BindEventBus.class);
    }

    /**
     * 注册EventBus
     */
    public void registerEventBus() {
        try {
            if (!EventBus.getDefault().isRegistered(this)) {
                EventBus.getDefault().register(this);
            }
        } catch (Exception e) {
            LogPlus.e(e.getMessage());
        }
    }

    /**
     * 反注册EventBus
     */
    public void unregisterEventBus() {
        if (EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().unregister(this);
        }
    }

    @Override
    public void startActivity(Class<? extends Activity> cls) {
        startActivity(new Intent(getActivity(), cls));
    }

    @Override
    public void startActivityForResult(Class<? extends Activity> cls, int requestCode) {
        startActivityForResult(new Intent(getActivity(), cls), requestCode);
    }

    @Override
    public void finishActivity() {
        try {
            getActivityView().finishActivity();
        } catch (Exception e) {
            //e.printStackTrace();
        }
    }

    @Override
    public void showToast(int resId) {
        try {
            getActivityView().showToast(resId);
        } catch (Exception e) {
            //e.printStackTrace();
        }
    }

    @Override
    public void showToast(String text) {
        try {
            getActivityView().showToast(text);
        } catch (Exception e) {
            //e.printStackTrace();
        }
    }

    @Override
    public void showOneToast(int resId) {
        try {
            getActivityView().showOneToast(resId);
        } catch (Exception e) {
            //e.printStackTrace();
        }
    }

    @Override
    public void showOneToast(String text) {
        try {
            getActivityView().showOneToast(text);
        } catch (Exception e) {
            //e.printStackTrace();
        }
    }

    @Override
    public void showWaitingDialog(String text, boolean cancelable) {
        try {
            getActivityView().showWaitingDialog(text, cancelable);
        } catch (Exception e) {
            //e.printStackTrace();
        }
    }

    @Override
    public void showWaitingDialog(int stringRes, boolean cancelable) {
        try {
            getActivityView().showWaitingDialog(stringRes, cancelable);
        } catch (Exception e) {
            //e.printStackTrace();
        }
    }

    @Override
    public void showWaitingDialog(String text) {
        try {
            getActivityView().showWaitingDialog(text);
        } catch (Exception e) {
            //e.printStackTrace();
        }
    }

    @Override
    public void showWaitingDialog(int stringRes) {
        try {
            getActivityView().showWaitingDialog(stringRes);
        } catch (Exception e) {
            //e.printStackTrace();
        }
    }

    @Override
    public void dismissWaitingDialog() {
        try {
            getActivityView().dismissWaitingDialog();
        } catch (Exception e) {
            //e.printStackTrace();
        }
    }

    protected UiView getActivityView() {
        return (UiView) mActivity;
    }

    @Override
    public long defaultWaitingDelay() {
        try {
            return getActivityView().defaultWaitingDelay();
        } catch (Exception e) {
            //e.printStackTrace();
            return 500L;
        }
    }

    @Override
    public void showDelayWaitingDialog(String text, long delay) {
        try {
            getActivityView().showDelayWaitingDialog(text, delay);
        } catch (Exception e) {
            //e.printStackTrace();
        }
    }

    @Override
    public void showDelayWaitingDialog(int stringRes, long delay) {
        try {
            getActivityView().showDelayWaitingDialog(stringRes, delay);
        } catch (Exception e) {
            //e.printStackTrace();
        }
    }

    @Override
    public void showDelayWaitingDialog(String text) {
        try {
            getActivityView().showDelayWaitingDialog(text);
        } catch (Exception e) {
            //e.printStackTrace();
        }
    }

    @Override
    public void showDelayWaitingDialog(int stringRes) {
        try {
            getActivityView().showDelayWaitingDialog(stringRes);
        } catch (Exception e) {
            //e.printStackTrace();
        }
    }

    @Override
    public void dismissDelayWaitingDialog() {
        try {
            getActivityView().dismissDelayWaitingDialog();
        } catch (Exception e) {
            //e.printStackTrace();
        }
    }
}
