package com.licheedev.base;

import android.app.Dialog;
import android.content.Context;
import android.view.Gravity;
import androidx.annotation.NonNull;
import com.licheedev.myutils.ui.DialogUtil;

public class CommonDialog extends Dialog {

    public CommonDialog(@NonNull Context context) {
        super(context, R.style.CenterDialogStyle);
        DialogUtil.setGravity(this, Gravity.CENTER);
    }
}
