package com.licheedev.base;

import android.app.Activity;
import android.os.Handler;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import androidx.annotation.NonNull;
import com.licheedev.base.core.WaitingDialog;

/**
 * 延时显示的对话框，显示时，先立即显示一个透明层覆盖在mDecorView上，延时过后才显示对话框
 */
public class DelayWaitingDialog extends FrameLayout implements WaitingDialog {

    private final WaitingDialog mDialog;
    private final ViewGroup mDecorView;

    private static final ViewGroup.LayoutParams MATCH_PARAMS =
        new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
            ViewGroup.LayoutParams.MATCH_PARENT);

    private final Handler mHandler = new Handler();

    private boolean mIsShowing;

    private long mDelay = 0;

    public DelayWaitingDialog(@NonNull Activity context, @NonNull WaitingDialog dialog) {
        super(context);

        mDecorView = (ViewGroup) context.getWindow().getDecorView();
        mDialog = dialog;

        ViewGroup.LayoutParams layoutParams =
            new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.MATCH_PARENT);

        setLayoutParams(layoutParams);

        // 停用点击音效
        setSoundEffectsEnabled(false);
        // 设置可点击，这样就能屏蔽盖在其下面的控件的事件了
        setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                // ignore
            }
        });

        mDialog.setCancelable(false);

        mIsShowing = false;
    }

    public void setDelay(long delay) {
        mDelay = delay;
    }

    public long getDelay() {
        return mDelay;
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        super.onTouchEvent(event);
        return true;
    }

    // 
    public void show() {
        mHandler.removeCallbacksAndMessages(null);
        showOverlay();
        if (mDelay <= 0) {
            showRealDialog();
            return;
        }

        mHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                showRealDialog();
            }
        }, mDelay);
    }

    public void dismiss() {
        mHandler.removeCallbacksAndMessages(null);
        try {
            mDialog.dismiss();
        } catch (Exception e) {
            //e.printStackTrace();
        }
        removeOverlay();
    }

    protected void showOverlay() {
        try {
            if (getParent() == null) {
                mDecorView.addView(this, MATCH_PARAMS);
            }
        } catch (Exception e) {
            //e.printStackTrace();
        } finally {
            mIsShowing = true;
        }
    }

    protected void removeOverlay() {
        try {
            int i = mDecorView.indexOfChild(this);
            //LogPlus.e(this.getClass().getSimpleName() + ",隐藏，index=" + i);
            if (i >= 0) {
                mDecorView.removeViewAt(i);
            }
        } catch (Exception e) {
            mIsShowing = false;
        }
    }

    public void showRealDialog() {
        try {
            if (!mDialog.isShowing()) {
                mDialog.show();
            }
        } catch (Exception e) {
            //e.printStackTrace();
        }
    }

    public WaitingDialog getRealDialog() {
        return mDialog;
    }

    @Override
    public WaitingDialog setMessage(int resId) {
        return mDialog.setMessage(resId);
    }

    @Override
    public WaitingDialog setMessage(CharSequence message) {
        return mDialog.setMessage(message);
    }

    public DelayWaitingDialog setCancelable(boolean cancelable) {
        // 空实现，默认不能取消
        //mDialog.setCancelable(false);
        return this;
    }

    public boolean isShowing() {
        return mIsShowing;
    }
}
